package fr.sandrineneveu.marsrover;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

public class RoverComplexeCommandTest {
	
	@Test
	void shouldErrorWhenRoverReceiveUnknownComplexeCommand(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "F,F,R,x,F,L";
	    
	    //WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.execute(command));
	    assertEquals("unknown command", exception.getMessage());
	    assertEquals(Direction.E, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(3, rover.getCoordinate().getY());
	}

	@Test
	void shouldRoverMoveWithComplexeCommand(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "F,F,R,F,L,B,B,B,L,B";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.W, rover.getDirection());
	    assertEquals(3, rover.getCoordinate().getX());
	    assertEquals(5, rover.getCoordinate().getY());
	}
	
	@Test
	void shouldRoverStopSequenceCommandWhenObstacleIsPresent(){
	    //GIVEN
		Rover rover = new Rover(getPlanetWithObstacles(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "F,F,R,F,L,B,B,B,L,B";
	    
	    //WHEN & THEN	   
	    assertThrows(RoverException.class, () -> rover.execute(command));
	    assertEquals(Direction.E, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(3, rover.getCoordinate().getY());
	}

	private Planet getPlanet(){
		return new Planet(5, 5, Collections.emptyList());
	}
	
	private Planet getPlanetWithObstacles() {
		List<Coordinate> obstaclesList = new ArrayList<Coordinate>();
		obstaclesList.add(Coordinate.builder().x(2).y(3).build());
		return new Planet(5, 5, obstaclesList);
	}
}

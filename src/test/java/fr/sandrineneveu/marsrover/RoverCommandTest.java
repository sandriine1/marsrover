package fr.sandrineneveu.marsrover;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;

import org.junit.jupiter.api.Test;

public class RoverCommandTest {
	
	@Test
	void shouldRoverTurnLeftWhenReceiveCommandL(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "L";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.W, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(1, rover.getCoordinate().getY());
	}
	
	@Test
	void shouldRoverTurnLeftWhenReceiveCommandl(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "l";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.W, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(1, rover.getCoordinate().getY());
	}

	@Test
	void shouldRoverTurnRightWhenReceiveCommandR(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "R";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.E, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(1, rover.getCoordinate().getY());
	}
	
	@Test
	void shouldRoverTurnRightWhenReceiveCommandr(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "r";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.E, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(1, rover.getCoordinate().getY());
	}
	
	@Test
	void shouldRoveMoveForwardWhenReceiveCommandF(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "F";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(2, rover.getCoordinate().getY());
	}

	@Test
	void shouldRoveMoveForwardWhenReceiveCommandf(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "f";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(2, rover.getCoordinate().getY());
	}

	@Test
	void shouldRoverMoveBackwardWhenReceiveCommandB(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "B";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(5, rover.getCoordinate().getY());
	}

	@Test
	void shouldRoverMoveBackwardWhenReceiveCommandb(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "b";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(5, rover.getCoordinate().getY());
	}
	
	@Test
	void shouldRoverMoveForwardWhenReceiveCommandFAndPositionIsMaxOfThePlanet(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(5).build(), Direction.N);
	    String command = "F";
	    
	    //WHEN
	    rover.execute(command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getCoordinate().getX());
	    assertEquals(1, rover.getCoordinate().getY());
	}

	@Test
	void shouldErrorWhenRoverReceiveUnknownSimpleCommand(){
	    //GIVEN
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), Direction.N);
	    String command = "x";
	    
	    //WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.execute(command));
	    assertEquals("unknown command", exception.getMessage());
	}

	private Planet getPlanet(){
		return new Planet(5, 5, Collections.emptyList());
	}
}

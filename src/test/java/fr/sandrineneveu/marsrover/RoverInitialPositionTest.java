package fr.sandrineneveu.marsrover;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;

import org.junit.jupiter.api.Test;

public class RoverInitialPositionTest {

	@Test
	void createRoverWithInitialPosition() {
		Rover rover = new Rover(getPlanet(), Coordinate.builder().x(1).y(4).build(), Direction.N);
		assertEquals(1, rover.getCoordinate().getX());
		assertEquals(4, rover.getCoordinate().getY());
		assertEquals(Direction.N, rover.getDirection());
	}
	
	@Test
	void shouldErrorWhenRoverInitialPositionIsWithoutplanet(){
	    //WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> new Rover(getPlanet(), Coordinate.builder().x(6).y(6).build(), Direction.N));
	    assertEquals("the rover is off the planet", exception.getMessage());
	}

	@Test
	void shouldErrorWhenCreateRoverWithoutplanet(){

	    //WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> new Rover(null, Coordinate.builder().x(1).y(1).build(), Direction.N));
	    assertEquals("it takes a planet to land", exception.getMessage());
	}
	
	@Test
	void shouldErrorWhenCreateRoverWithEmptyX() {

		//WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> new Rover(getPlanet(), Coordinate.builder().y(1).build(), Direction.N));
	    assertEquals("we need initial position to land the rover", exception.getMessage());
	}
	
	@Test
	void shouldErrorWhenCreateRoverWithEmptyY() {

		//WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> new Rover(getPlanet(), Coordinate.builder().x(1).build(), Direction.N));
	    assertEquals("we need initial position to land the rover", exception.getMessage());
	}
	
	@Test
	void shouldErrorWhenCreateRoverWithEmptyDirection() {

		//WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> new Rover(getPlanet(), Coordinate.builder().x(1).y(1).build(), null));
	    assertEquals("we need initial position to land the rover", exception.getMessage());
	}

	private Planet getPlanet(){
		return new Planet(5, 5, Collections.emptyList());
	}
}

package fr.sandrineneveu.marsrover;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Planet {

	final Integer xDimension;
	final Integer yDimension;
	final List<Coordinate> obstacles;
	
	public Planet(int xDimension, int yDimension, List<Coordinate> obstacles) {
		this.xDimension = xDimension;
		this.yDimension = yDimension;
		this.obstacles = Optional.ofNullable(obstacles).orElse(Collections.emptyList());
	}
	
	boolean contains(Coordinate coordinate) {
		return coordinate.x > 0 && coordinate.y > 0 && coordinate.x <= xDimension && coordinate.y <= yDimension;
	}
}

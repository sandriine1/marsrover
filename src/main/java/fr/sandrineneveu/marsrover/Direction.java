package fr.sandrineneveu.marsrover;

public enum Direction {
	
	N{
		@Override
		Direction turnLeft() {
			return Direction.W;
		}

		@Override
		Direction turnRight() {
			return Direction.E;
		}
		
	},
	E{
		@Override
		Direction turnLeft() {
			return Direction.N;
		}

		@Override
		Direction turnRight() {
			return Direction.S;
		}
	},
	S{
		@Override
		Direction turnLeft() {
			return Direction.E;
		}

		@Override
		Direction turnRight() {
			return Direction.W;
		}
	},
	W{
		@Override
		Direction turnLeft() {
			return Direction.S;
		}

		@Override
		Direction turnRight() {
			return Direction.N;
		}
	};
	
	abstract Direction turnLeft();
	abstract Direction turnRight();
}

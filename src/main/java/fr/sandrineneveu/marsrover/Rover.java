package fr.sandrineneveu.marsrover;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class Rover implements Command{
	
	private Coordinate coordinate;
	private Integer maxX;
	private Integer maxY;
	private Direction direction;
	private Planet planet;
	
	public Rover(Planet planet, Coordinate coordinate, Direction direction) {
		this.planet = planet;
		this.coordinate = coordinate;
		this.direction = direction;
		checkValues(this.planet, this.coordinate, this.direction);
		this.maxX = planet.xDimension;
		this.maxY = planet.yDimension;
	}
	
	private static void checkValues(Planet planet, Coordinate coordinate, Direction direction) {
		if(coordinate == null || coordinate.x == null || coordinate.y == null || direction == null) {
			throw new RoverException("we need initial position to land the rover");
		}
		if(planet == null) {
			throw new RoverException("it takes a planet to land");
		}
		if(!planet.contains(coordinate)) {
			throw new RoverException("the rover is off the planet");
		}
	}

	@Override
	public void execute(String command) {
		if(command == null || command.isEmpty()) {
			throw new RoverException("we need command for the rover");
		}else {
			List<String> commandsList = new ArrayList<>(Arrays.asList(command.toUpperCase().split(",")));
            for (String c : commandsList) {
                if(ValidCommand.RIGHT.getCommand().equals(c) || ValidCommand.LEFT.getCommand().equals(c)){
                    turn(c);
                }else if(ValidCommand.FORWARD.getCommand().equals(c) || ValidCommand.BACKWARD.getCommand().equals(c)){
                    go(ValidCommand.getValidCommand(c).orElseThrow(() -> new RoverException("unknown command")));
                }else{
                    throw new RoverException("unknown command");
                }
            }
		}
	}

	private void turn(String command) {
		if(ValidCommand.LEFT.getCommand().equals(command)) {
			this.direction = this.direction.turnLeft();
		}else if(ValidCommand.RIGHT.getCommand().equals(command)) {
			this.direction = this.direction.turnRight();
		}	
	}

	private void go(ValidCommand command) {
		Coordinate nextCoordinate = null;
		switch(this.direction){
	        case N:	        	
	        	nextCoordinate = this.coordinate.translate(ValidCommand.FORWARD.equals(command) ? Vector.UP : Vector.DOWN);
	        	break;
	        case E:
	        	nextCoordinate = this.coordinate.translate(ValidCommand.FORWARD.equals(command) ? Vector.RIGHT : Vector.LEFT);
	        	break;
	        case S:
	        	nextCoordinate = this.coordinate.translate(ValidCommand.FORWARD.equals(command) ? Vector.DOWN : Vector.UP);
	        	break;
	        case W:
	        	nextCoordinate = this.coordinate.translate(ValidCommand.FORWARD.equals(command) ? Vector.LEFT : Vector.RIGHT);
	        	break;
		}
		
		if(hasObstacle(nextCoordinate)){
			throw new RoverException("obstacle is present, rover stop to move");
		}else if(!positionIsGoodOnPlanet(nextCoordinate)) {
			this.move(nextCoordinate);
		}
	}
	
	private void move(Coordinate nextCoordinate) {
		if(Direction.N.equals(this.direction) || Direction.S.equals(this.direction)) {
			this.coordinate.y = nextCoordinate.y;
		}else if(Direction.E.equals(this.direction) || Direction.W.equals(this.direction)) {
			this.coordinate.x = nextCoordinate.x;
		}
	}
	
	private boolean positionIsGoodOnPlanet(Coordinate nextCoordinate){
		if(Direction.E.equals(this.direction) || Direction.W.equals(this.direction)) {
			if(nextCoordinate.x == 0) {
				this.coordinate.x = maxX;
				return true;
			}else if(nextCoordinate.x > this.maxX) {
				this.coordinate.x = 1;
				return true;
			}
		}
		
		if(Direction.N.equals(this.direction) || Direction.S.equals(this.direction)) {
			if(nextCoordinate.y == 0) {
				this.coordinate.y = maxY;
				return true;
			}else if(nextCoordinate.y > this.maxY) {
				this.coordinate.y = 1;
				return true;
			}
		}
		return false;
	}
	
	private boolean hasObstacle(Coordinate coordinate) {
		return this.planet.obstacles.contains(coordinate);
	}

}

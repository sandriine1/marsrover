package fr.sandrineneveu.marsrover;

public class RoverException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public RoverException (String s) {
	       super(s) ;
	   }

}

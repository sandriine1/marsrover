package fr.sandrineneveu.marsrover;

import java.util.Arrays;
import java.util.Optional;

public enum ValidCommand {
	
    RIGHT("R"), LEFT("L"), FORWARD("F"), BACKWARD("B") ;  
    
    private String command ;  
     
    private ValidCommand(String command) {  
        this.command = command ;  
   }  
     
    public String getCommand() {  
        return  this.command ;  
   }  
    
   public static Optional<ValidCommand> getValidCommand(String command) {
	   return Arrays.stream(values())
       .filter(validCommand -> validCommand.command.equals(command))
       .findFirst();
   }
    
}

package fr.sandrineneveu.marsrover;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class Coordinate {
	
	Integer x;
	Integer y;
	
	Coordinate translate(Vector vector) {
		return new Coordinate(this.x + vector.x, this.y + vector.y);
	}
}

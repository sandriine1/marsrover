package fr.sandrineneveu.marsrover;

public interface Command {

	void execute(String command);

}
